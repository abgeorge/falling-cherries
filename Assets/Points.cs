﻿using UnityEngine;
using System.Collections;

public class Points : MonoBehaviour {

	public static Points S;
	public int points = 0;
	public bool changeColor1;
	public bool changeColor2;
	public int colorsChanged;

	void Awake() {
		S = this;
	}

	// Use this for initialization
	void Start () {

		gameObject.GetComponent<GUIText> ().text = "Score: " + points.ToString();
		changeColor1 = false;
		changeColor2 = false;
		colorsChanged = 0;
	
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddPoints() {

		++points;
		gameObject.GetComponent<GUIText> ().text = "Score: " + points.ToString();
	}
}
