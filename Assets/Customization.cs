﻿using UnityEngine;
using System.Collections;

public class Customization : MonoBehaviour {


    // Use this for initialization
    void Start () {
}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.G))
        {
            Camera.main.backgroundColor = Color.green;
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            Camera.main.backgroundColor = Color.blue;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Camera.main.backgroundColor = Color.red;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            Camera.main.backgroundColor = Color.black;
        }

    }
}
