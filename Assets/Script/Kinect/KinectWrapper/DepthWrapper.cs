using UnityEngine;
using System;
using System.Collections;
using System.Linq;

/// <summary>
/// Level of indirection for the depth image,
/// provides:
/// -a frames of depth image (no player information),
/// -an array representing which players are detected,
/// -a segmentation image for each player,
/// -bounds for the segmentation of each player.
/// </summary>
public class DepthWrapper: MonoBehaviour {
    public const int RESOLUTIONX = 320;
    public const int RESOLUTIONY = 240;
    Camera main;
	public DeviceOrEmulator devOrEmu;
	private Kinect.KinectInterface kinect;

    public GameObject rectangle;
	
	private struct frameData
	{
		public short[] depthImg;
		//public bool[] players;
		public bool[,] segmentation;
		public int[,] bounds;
	}
	
	public int storedFrames = 1;
	
	private bool updatedSeqmentation = false;
	private bool newSeqmentation = false;
	
	private Queue frameQueue;
	
	/// <summary>
	/// Depth image for the latest frame
	/// </summary>
	[HideInInspector]
	public short[] depthImg;
	/// <summary>
	/// players[i] true iff i has been detected in the frame
	/// </summary>
	//[HideInInspector]
	//public bool[] players;
	/// <summary>
	/// Array of segmentation images [player, pixel]
	/// </summary>
	[HideInInspector]
	public bool[,] segmentations;
	/// <summary>
	/// Array of bounding boxes for each player (left, right, top, bottom)
	/// </summary>
	[HideInInspector]
	//right,left,up,down : but the image is fliped horizontally.
	public int[,] bounds;

    public int count = 0;
    public int frameCount = 0;
    public int avgIndex = 0; 
    public short[] initialDepths = new short[RESOLUTIONX * RESOLUTIONY];
    public short[] avgDepths = new short[RESOLUTIONX * RESOLUTIONY];

	// Use this for initialization
	void Start () {

		kinect = devOrEmu.getKinect();

        //allocate space to store the data of storedFrames frames.
        frameQueue = new Queue(storedFrames);
		for(int ii = 0; ii < storedFrames; ii++){	
			frameData frame = new frameData();
			frame.depthImg = new short[RESOLUTIONX * RESOLUTIONY];
			//frame.players = new bool[Kinect.Constants.NuiSkeletonCount];
			frame.segmentation = new bool[Kinect.Constants.NuiSkeletonCount, RESOLUTIONX * RESOLUTIONY];
			frame.bounds = new int[Kinect.Constants.NuiSkeletonCount,4];
			frameQueue.Enqueue(frame);
		}

    }
	
	// Update is called once per frame 
	void Update () {
	   
    }

    void LateUpdate()
	{
		updatedSeqmentation = false;
		newSeqmentation = false;
	}
	/// <summary>
	/// First call per frame checks if there is a new depth image and updates,
	/// returns true if there is new data
	/// Subsequent calls do nothing have the same return as the first call.
	/// </summary>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public bool pollDepth()
	{
		//Debug.Log("" + updatedSeqmentation + " " + newSeqmentation);
		if (!updatedSeqmentation)
		{
			updatedSeqmentation = true;
			if (kinect.pollDepth())
			{
				newSeqmentation = true;
				frameData frame = (frameData)frameQueue.Dequeue();
				depthImg = frame.depthImg;
				//players = frame.players;
				segmentations = frame.segmentation;
				bounds = frame.bounds;
				frameQueue.Enqueue(frame);
				processDepth();
			}
		}
		return newSeqmentation;
	}
    
    private void processDepth()
	{
        int xx = 0;
        int yy = 0;

       int maxDepth = 0;
       int maxDepthIndex = -1;

        if (frameCount % 10 == 0 && frameCount != 0)
        {
            for (int ii = 0; ii < RESOLUTIONX * RESOLUTIONY; ii++)
            {
                int diff = (initialDepths[ii] - avgDepths[ii]);

                if ((diff > maxDepth  && diff < 150 && avgDepths[ii] > 500 && avgDepths[ii] < 1000 ) || maxDepthIndex == -1)
                {
                    if (maxDepthIndex != 1)
                    {
                        maxDepth = diff;
                    }
                    maxDepthIndex = ii;

                }
            }

            xx = 320 - (2 * (maxDepthIndex % RESOLUTIONX));
            yy = 240 - (maxDepthIndex / RESOLUTIONX);

            //print("point is " + xx + ", " + yy + " and diff is " + maxDepth);

            if (GameObject.Find("Rectangle(Clone)"))
            {
                Destroy(GameObject.Find("Rectangle(Clone)"));
            }

             Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(xx, yy, 0));
             point = new Vector3(point.x + 13f, point.y, 0);
             Instantiate(rectangle, new Vector3(point.x, point.y, 0), Quaternion.identity);



            Array.Clear(avgDepths, 0, avgDepths.Length);
        } else if (frameCount == 0) {
            for (int ii = 0; ii < RESOLUTIONX * RESOLUTIONY; ii++)
            {
                initialDepths[ii] = (short)(kinect.getDepth()[ii] >> 3);
                avgDepths[ii] = 0;
            }
            ++count;
            if(Math.Abs((short)initialDepths[34890] - kinect.getDepth()[34890]) == 0) {
                print("RESTART THIS IT'S NOT WORKING");
            }
        }
        else
        {
            for (int ii = 0; ii < RESOLUTIONX * RESOLUTIONY; ii++)
            {
                short depth = (short)(kinect.getDepth()[ii] >> 3);
                avgDepths[ii] = (short)(((short)avgDepths[ii] * (short)(frameCount % 10) + depth) / ((short)(frameCount % 10) + 1));
                int diff = Math.Abs(((avgDepths[ii]) - (int)initialDepths[ii]));

            }
        }

        ++frameCount;

    }

}

