﻿using UnityEngine;
using System.Collections;

public class ShootingStar : MonoBehaviour {

	public float speed = 30f;
	public Vector3 originalPos;
	int count = 0;
	public bool moving;
	public int chance;

	// Use this for initialization
	void Start () {
		originalPos = transform.position;
		moving = false;
		chance = Random.Range (1, 50000);
	}

	void OnMouseDown() {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				Points.S.AddPoints ();
				Reset ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (!moving) {
			++count;
			if (count >= chance) {
				moving = true;
				count = 0;
				chance = Random.Range (1, 50000);
			}
		} else if (moving) {
			if (transform.position.x < 20f) {
				transform.Translate(Vector3.right * speed * Time.deltaTime);
			}
			else {
				Reset ();
			}
		}
	}

	void Reset() {
		transform.position = originalPos;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moving = false;
	}
}
