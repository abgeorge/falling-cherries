﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class spaceshipMovement : MonoBehaviour {

	public float speed;
	public Vector3 moveVec;
	public Vector3 originalPos;

	private Color startcolor;
	private RaycastHit2D hit;

	public int chance;
	public int count = 0;
	public bool moving = false;

	// Use this for initialization
	void Start () {
		speed = (float)(Random.Range (1, 8));
		originalPos = transform.position;
		startcolor = Color.gray;
		GetComponent<Renderer> ().material.color = startcolor;
		chance = Random.Range (1, 50000); 
	}

	void OnMouseDown() {
		if (Input.GetMouseButtonDown (0) && moving) {
			hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			
			if (hit.collider != null) {
				moveVec.x = 0;
				moveVec.y = 0;
				moveVec.z = 0;
				speed = 0;
				if(hit.collider.CompareTag("Test1")) {
					Points.S.changeColor2 = true;
				}
				if(hit.collider.CompareTag("Test2")) {
					Points.S.changeColor1 = true;
				}

			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (moving) {
			moveVec += Vector3.down * speed;
			
			if (transform.position.y <= -12f || transform.position.x >= 20f || transform.position.x <= -20f) {
				Reset ();	
			}
			
			transform.Translate (moveVec.normalized * 5f * Time.fixedDeltaTime);

			if (Points.S.changeColor1 && this.CompareTag("Test1")) {
				GetComponent<Renderer> ().material.color = Color.green;
				moveVec.x = 0;
				moveVec.y = 0;
				moveVec.z = 0;
				speed = 0;
			} else if (Points.S.changeColor2 && this.CompareTag("Test2")) {
				GetComponent<Renderer> ().material.color = Color.green;
				moveVec.x = 0;
				moveVec.y = 0;
				moveVec.z = 0;
				speed = 0;
			}

			if (Points.S.colorsChanged > 0) {
				GetComponent<Renderer> ().material.color = startcolor;
				Points.S.colorsChanged = Points.S.colorsChanged - 1;
				moving = false;
				Reset ();
			}

			if (Points.S.changeColor1 && Points.S.changeColor2) {
				Points.S.AddPoints();
				Points.S.colorsChanged = 2;
				Points.S.changeColor1 = false;
				Points.S.changeColor2 = false;
			}
		}
		else {
			++count;
			if (count >= chance) {
				print ("spaceships go!");
				GameObject[] gos = FindObjectsOfType(typeof(GameObject)) as GameObject[];
				foreach (GameObject go in gos) {
					if (go.layer == 11) {
						print (go.name + " GO!");
						go.GetComponent<spaceshipMovement>().moving = true;
						go.GetComponent<spaceshipMovement>().count = 0;
						go.GetComponent<spaceshipMovement>().chance = Random.Range(0, 50000);
					}
				}
			}
		}
	}

	public void Reset() {
		speed = (float)(Random.Range (1, 8));
		transform.position = new Vector3(originalPos.x, originalPos.y, originalPos.z);
		transform.rotation = Quaternion.Euler(0, 0, 0);
		if (!(Points.S.changeColor1 || Points.S.changeColor2)) {
			GetComponent<Renderer> ().material.color = startcolor;
		}
	}

}
