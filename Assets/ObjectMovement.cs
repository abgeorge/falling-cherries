﻿using UnityEngine;
using System.Collections;

public enum Direction {
	left,
	right
}

public class ObjectMovement : MonoBehaviour {


    public static ObjectMovement S;

	public float speed;
	public Direction direction;
	public Vector3 moveVec;
	public Vector3 originalPos;

	private Color startcolor;

    public GameObject rectangle;

    void Awake()
    {
        S = this;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name.ToString() == "Rectangle(Clone)")
        {
            print(this.gameObject.name + " was hit by " + coll.gameObject.name);
            this.Tap();
        }
    }


	// Use this for initialization
	void Start () {
		direction = (Direction)(Random.Range (0, 2));
		speed = (float)(Random.Range (1, 8));
		originalPos = transform.position;
		startcolor = GetComponent<Renderer>().material.color;

	}

    public void Tap()
    {
        
        Points.S.AddPoints();
        Reset();
    }
	
	// Update is called once per frame
	void Update () {
		moveVec += Vector3.down;

		if (transform.position.y <= -12f || transform.position.x >= 20f || transform.position.x <= -20f) {
			Reset ();

		} else if (direction == Direction.left) {
			moveVec += Vector3.left;
		} else if (direction == Direction.right) {
			moveVec += Vector3.right;
		}

		transform.Translate (moveVec.normalized * 5f * Time.fixedDeltaTime);

	}

	void Reset() {
		direction = (Direction)(Random.Range (0, 2));
		speed = (float)(Random.Range (1, 8));
		transform.position = new Vector3(originalPos.x, originalPos.y, originalPos.z);
		transform.rotation = Quaternion.Euler(0, 0, 0);
	}
}
